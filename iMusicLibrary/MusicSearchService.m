//
//  MusicSearchService.m
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 06/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import "MusicSearchService.h"

@implementation MusicSearchService

@synthesize searchTerm;

@synthesize searchMethod;

@synthesize mBidId;

@synthesize album;
@synthesize artist;
@synthesize track;

@synthesize delegate;

@synthesize results;

@synthesize details;
@synthesize tracklists;

-(void)main
{
    NSString *url;
    
    NSString *api_key = @"a3101c11bac542bcfea73536cd63bc5e";
    NSString *search_term = [searchTerm stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSString *mbid_id = [mBidId stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    if([searchMethod isEqualToString:@"album"])
    {
    
        url = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=album.search&api_key=%@&album=%@&format=json", api_key, search_term];
    }
    else if([searchMethod isEqualToString:@"album.getinfo"])
    {
        if(mbid_id.length > 0)
        {
            url = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=%@&mbid=%@&format=json", api_key, mbid_id];
        }
        else if (mbid_id.length == 0)
        {
            NSString *album_name = [album stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
            NSString *artist_name = [artist stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
            url = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=%@&album=%@&artist=%@&format=json", api_key, album_name, artist_name];
        }
    }
    else if([searchMethod isEqualToString:@"track"])
    {
        url = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=track.search&api_key=%@&track=%@&format=json", api_key, search_term];
    }
    else if([searchMethod isEqualToString:@"track.getinfo"])
    {
        if(mbid_id != (id)[NSNull null] || mbid_id.length > 0)
        {
            url = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=album.search&api_key=%@&mbid=%@&format=json", api_key, mbid_id];
        }
        else if (mbid_id == (id)[NSNull null] || mbid_id.length == 0)
        {
            
            NSString *track_name = [album stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
            NSString *artist_name = [artist stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
            url = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=album.search&api_key=%@&track=%@&artist=%@&format=json", api_key, track_name, artist_name];
        }
    }
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:nil];
    
    if(responseData != nil)
    {
        NSError *error = nil;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        if(error)
        {
            [delegate serviceFinished:self WithError:YES];
        }
        else
        {
            if([searchMethod isEqualToString:@"album"])
            {
                
                results = (NSArray *)[[[json valueForKey:@"results"] valueForKey:@"albummatches"] valueForKey:@"album"];
            }
            else if([searchMethod isEqualToString:@"album.getinfo"])
            {
                details = (NSDictionary *) [json valueForKey:@"album"];
                tracklists = [(NSArray *) [[json valueForKey:@"album"] valueForKey:@"tracks"] valueForKey:@"track"];
            }
            else if([searchMethod isEqualToString:@"track"])
            {
                results = (NSArray *)[[[json valueForKey:@"results"] valueForKey:@"trackmatches"] valueForKey:@"track"];
            }
            else if([searchMethod isEqualToString:@"track.getinfo"])
            {
                //results = (NSArray *)[json valueForKey:@"track"];
                details = (NSDictionary *) [json valueForKey:@"track"];
            }
            
            [delegate serviceFinished:self WithError:NO];
        }
    }
    else
    {
        [delegate serviceFinished:self WithError:YES];
    }
}
@end
