//
//  ServiceDelegate.h
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 06/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServiceDelegate <NSObject>

-(void)serviceFinished:(id)service WithError:(BOOL)error;

@end
