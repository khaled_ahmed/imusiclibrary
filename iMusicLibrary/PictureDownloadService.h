//
//  PictureDownloadService.h
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 14/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceDelegate.h"
@interface PictureDownloadService : NSOperation
{
    NSString *albumId;
    NSString *albumPictureUrl;
    id<ServiceDelegate> delegate;
}

@property (nonatomic, retain) NSString *albumId;
@property (nonatomic, retain) NSString *albumPictureUrl;
@property (nonatomic, retain) id<ServiceDelegate> delegate;

@end
