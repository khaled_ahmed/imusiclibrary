//
//  MusicDetailsViewController.m
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 10/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import "MusicDetailsViewController.h"
#import "MusicSearchService.h"
#import "ServiceDelegate.h"
#import "MusicTableViewController.h"

@interface MusicDetailsViewController ()

@end

@implementation MusicDetailsViewController

@synthesize imgAlbumArt;
@synthesize LblalbumName;
@synthesize LblartistName;
@synthesize LblRealeaseYear;
@synthesize LblStatus;

@synthesize AlbumTacksList;

@synthesize albumtracks;

@synthesize album;

@synthesize statusList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        [[self navigationItem] setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshAlbumDetails)]];
        
    }
    
    return self;
}

-(void)dealloc
{
    pickerViewPopup = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    if([self album] != nil)
    {
        //============================================================
        [self setTitle:[[self album] valueForKey:@"name"]];
        
        [LblalbumName setText:[[self album] valueForKey:@"name"]];
        [LblartistName setText:[[self album] valueForKey:@"artist"]];
        [LblStatus setText:[[self album] valueForKey:@"status"]];
        
        // service operation
        serviceQueue = [[NSOperationQueue alloc] init];
        [serviceQueue setMaxConcurrentOperationCount:1];
        
        if(![[[self album] allKeys] containsObject:@"releasedate"])
        {
            MusicSearchService *service = [[MusicSearchService alloc] init];
        
            // set the search method name
            [service setSearchMethod:@"album.getinfo"];
            [service setMBidId:[[self album] valueForKey:@"mbid"]];
            [service setAlbum:[[self album] valueForKey:@"name"]];
            [service setArtist:[[self album] valueForKey:@"artist"]];
        
            [service setDelegate:self];
        
            [serviceQueue addOperation:service];
        }
        
        // initialise the date format for release date
        NSString *str_date = [[NSString alloc] init];
        str_date = [[self album] valueForKey:@"releasedate"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"d MMMM yyyy, h:00"];
        NSDate *label_date = [formatter dateFromString:str_date];
        
        [formatter setDateFormat:@"d MMMM yyyy"];
        NSString *date = [formatter stringFromDate:label_date];
        
        [LblRealeaseYear setText:date];
        
        // check if the image is downloaded
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@.png", docDir, [[self album] valueForKey:@"id"]];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if([fileManager fileExistsAtPath:pngFilePath])
        {
            UIImage *pic = [UIImage imageWithData:[NSData dataWithContentsOfFile:pngFilePath]];
            [imgAlbumArt setImage:pic];
        }
        else
        {
            PictureDownloadService *service = [[PictureDownloadService alloc] init];
            [service setAlbumId:[[[self album] valueForKey:@"id"] description]];
            [service setDelegate:self];
            [serviceQueue addOperation:service];
        }
        [[self AlbumTacksList] reloadData];

    }
}
-(void)refreshView
{
    if([self album] != nil)
    {
        [self setTitle:[[self album] valueForKey:@"name"]];
        
        [LblalbumName setText:[[self album] valueForKey:@"name"]];
        [LblartistName setText:[[self album] valueForKey:@"artist"]];
        [LblStatus setText:[[self album] valueForKey:@"status"]];
        
        
        // initialise the date format for release date
        NSString *str_date = [[NSString alloc] init];
        str_date = [[self album] valueForKey:@"releasedate"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"d MMMM yyyy, h:00"];
        NSDate *label_date = [formatter dateFromString:str_date];
        
        [formatter setDateFormat:@"d MMMM yyyy"];
        NSString *date = [formatter stringFromDate:label_date];
        
        [LblRealeaseYear setText:date];
        
        // check if the image is downloaded
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@.png", docDir, [[self album] valueForKey:@"id"]];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if([fileManager fileExistsAtPath:pngFilePath])
        {
            UIImage *pic = [UIImage imageWithData:[NSData dataWithContentsOfFile:pngFilePath]];
            [imgAlbumArt setImage:pic];
        }
    }
}

-(void)refreshAlbumDetails
{
    MusicSearchService *service = [[MusicSearchService alloc] init];
    [service setMBidId:[[self album] valueForKey:@"mbid"]];
    [service setAlbum:[[self album] valueForKey:@"name"]];
    [service setArtist:[[self album] valueForKey:@"artist"]];
    [service setDelegate:self];
    [serviceQueue addOperation:service];
}

-(IBAction)changeStatus:(id)sender
{
    // initialise the status list
    statusList = [[NSMutableArray alloc]init];
    
    [statusList addObject:@"I want it"];
    [statusList addObject:@"I Own it"];
    
    pickerViewPopup = [[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    
    statusPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    
    [statusPickerView setDelegate:self];
    [statusPickerView setDataSource:self];
    [statusPickerView setShowsSelectionIndicator:YES];
    
    // uibarbuttonitem
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    [pickerToolbar setBarStyle:UIBarStyleDefault];
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *buttonItems = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [barItems addObject:buttonItems];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePickingStatus:)];
    
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    
    [barItems addObject:btnDone];
    [barItems addObject:btnCancel];
    [pickerToolbar setItems:barItems animated:YES];
    
    [pickerViewPopup addSubview:pickerToolbar];
    [pickerViewPopup addSubview:statusPickerView];
    
    [pickerViewPopup showInView:self.view];
    [pickerViewPopup setBounds:CGRectMake(0, 0, 320, 485)];
    
}

// return the number of columns to display
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// return the # of rows in each component...
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [statusList count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //return [NSString stringWithFormat:@"%i", row];
    return [statusList objectAtIndex:row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // set album status
    [[self album] setValue:[statusList objectAtIndex:row] forKey:@"status"];
    
}

-(void)donePickingStatus:(id)sender
{
    // dismis the picker view
    [pickerViewPopup dismissWithClickedButtonIndex:0 animated:YES];
    
    pickerViewPopup = nil;
    
    statusPickerView = nil;
    
    // set the status value
    [LblStatus setText:[[self album] valueForKey:@"status"]];
}

-(void)cancel:(id)sender
{
    [pickerViewPopup dismissWithClickedButtonIndex:0 animated:YES];
}


-(void)saveAlbum
{
    // save the changes to the  database
    // store data
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *yourArrayFileName = [documentsDirectory stringByAppendingPathComponent:@"Albums.xml"];
    [album writeToFile:yourArrayFileName atomically:YES];
}

#pragma mark Table view control
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return 10;
    if([albumtracks count] == 0)
    {
        return 10;
    }
    else
    {
        return [albumtracks count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Identifier for retrieving reusable cells.
    static NSString *cellIdentifier = @"MyCellIdentifier"; // Attempt to request the reusable cell.
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    // No cell available - create one.
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    // Set the text of the cell to the row index.
    //cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row];
    NSLog(@"%@", [albumtracks valueForKey:@"name"]);
    
    NSDictionary *t = [[albumtracks valueForKey:@"name"] objectAtIndex:[indexPath row]];
    
    [[cell textLabel] setText:[t valueForKey:@"name"]];
    
    return cell;
}


#pragma mark service controll
-(void)serviceFinished:(id)service WithError:(BOOL)error
{
    // enter service response code
    if (!error)
    {
        if([service class] == [MusicSearchService class])
        {
            NSDictionary *music = [service details];
            [[self album] setValue:[music valueForKey:@"releasedate"] forKey:@"releasedate"];
            
            for(NSDictionary *_tracks in [service tracklists])
            {
                [albumtracks addObject:[_tracks valueForKey:@"name"]];
            }
            [[self AlbumTacksList] reloadData];
            
            // download cache album art image
            PictureDownloadService *service = [[PictureDownloadService alloc] init];
            [service setDelegate:self];
            [service setAlbumId:[[self album] valueForKey:@"id"]];
            [service setAlbumPictureUrl: [[self album]valueForKey:@"image"]];
            [serviceQueue addOperation:service];
            
            
            // refresh the UI in Main Thread
            [self performSelectorOnMainThread:@selector(refreshView) withObject:nil waitUntilDone:YES];
        }
        else
        {
            // refresh the UI in Main Thread
            [self performSelectorOnMainThread:@selector(refreshView) withObject:nil waitUntilDone:YES];
        }
    }
    else
    {
        /*
        [searchResults removeAllObjects];
        [searchResults addObject:
         [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"-1", @"There has been an Error", @"", nil] forKeys:[NSArray arrayWithObjects:@"id", @"name", @"artist",nil]]];
        [[self tableView] reloadData];
        */
    }

}

@end
