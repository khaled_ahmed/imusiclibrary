//
//  TrackDetailsViewController.m
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 11/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import "TrackDetailsViewController.h"
#import "MusicSearchService.h"
#import "ServiceDelegate.h"

@interface TrackDetailsViewController ()

@end

@implementation TrackDetailsViewController

@synthesize LblTrackName;
@synthesize imgAlbumArt;
@synthesize LblalbumName;
@synthesize LblartistName;
@synthesize LblRealeaseDate;
@synthesize LblStatus;


@synthesize AlbumTacksList;

@synthesize Track;

@synthesize statusList;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[self navigationItem] setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshTrackDetails)]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if([self Track] != nil)
    {
        [self setTitle:[[self Track] valueForKey:@"name"]];
        [LblTrackName setText:[[self Track] valueForKey:@"name"]];
        [LblalbumName setText:[[self Track] valueForKey:@"album"]];
        [LblartistName setText:[[self Track] valueForKey:@"artist"]];
        [LblStatus setText:[[self Track] valueForKey:@"status"]];
        
        // service operation
        serviceQueue = [[NSOperationQueue alloc] init];
        [serviceQueue setMaxConcurrentOperationCount:1];
        
        if(![[[self Track] allKeys] containsObject:@"releasedate"])
        {
            MusicSearchService *service = [[MusicSearchService alloc] init];
            
            // set the search method name
            [service setSearchMethod:@"album.getinfo"];
            [service setMBidId:[[self Track] valueForKey:@"mbid"]];
            [service setAlbum:[[self Track] valueForKey:@"name"]];
            [service setArtist:[[self Track] valueForKey:@"artist"]];
            
            [service setDelegate:self];
            
            [serviceQueue addOperation:service];
        }
        
        // initialise the date format for release date
        NSString *str_date = [[NSString alloc] init];
        str_date = [[self Track] valueForKey:@"releasedate"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"d MMMM yyyy, h:00"];
        NSDate *label_date = [formatter dateFromString:str_date];
        
        [formatter setDateFormat:@"d MMMM yyyy"];
        NSString *date = [formatter stringFromDate:label_date];
        
        [LblRealeaseDate setText:date];
        
        // check if the image is downloaded
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@.png", docDir, [[self Track] valueForKey:@"id"]];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if([fileManager fileExistsAtPath:pngFilePath])
        {
            UIImage *pic = [UIImage imageWithData:[NSData dataWithContentsOfFile:pngFilePath]];
            [imgAlbumArt setImage:pic];
        }
        else
        {
            PictureDownloadService *service = [[PictureDownloadService alloc] init];
            [service setAlbumId:[[[self Track] valueForKey:@"id"] description]];
            [service setDelegate:self];
            [serviceQueue addOperation:service];
        }
    }
}

-(void)refreshView
{
    if([self Track] != nil)
    {
        [self setTitle:[[self Track] valueForKey:@"name"]];
        [LblTrackName setText:[[self Track] valueForKey:@"name"]];
        [LblalbumName setText:[[self Track] valueForKey:@"album"]];
        [LblartistName setText:[[self Track] valueForKey:@"artist"]];
        [LblStatus setText:[[self Track] valueForKey:@"status"]];
        
        // initialise the date format for release date
        NSString *str_date = [[NSString alloc] init];
        str_date = [[self Track] valueForKey:@"releasedate"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"d MMMM yyyy, h:00"];
        NSDate *label_date = [formatter dateFromString:str_date];
        
        [formatter setDateFormat:@"d MMMM yyyy"];
        NSString *date = [formatter stringFromDate:label_date];
        
        [LblRealeaseDate setText:date];
        
        // check if the image is already downloaded
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@.png", docDir, [[self Track] valueForKey:@"id"]];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if([fileManager fileExistsAtPath:pngFilePath])
        {
            UIImage *pic = [UIImage imageWithData:[NSData dataWithContentsOfFile:pngFilePath]];
            [imgAlbumArt setImage:pic];
        }
    }
}


-(void)refreshTrackDetails
{
    MusicSearchService *service = [[MusicSearchService alloc] init];
    [service setMBidId:[[self Track] valueForKey:@"mbid"]];
    [service setTrack:[[self Track] valueForKey:@"name"]];
    [service setAlbum:[[self Track] valueForKey:@"album"]];
    [service setArtist:[[self Track] valueForKey:@"artist"]];
    [service setDelegate:self];
    [serviceQueue addOperation:service];
}

-(IBAction)changeStatus:(id)sender
{
    // initialise the status list
    statusList = [[NSMutableArray alloc]init];
    
    [statusList addObject:@"I want it"];
    [statusList addObject:@"I Own it"];
    
    pickerViewPopup = [[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    
    statusPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    
    [statusPickerView setDelegate:self];
    [statusPickerView setDataSource:self];
    [statusPickerView setShowsSelectionIndicator:YES];
    
    // uibarbuttonitem
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    [pickerToolbar setBarStyle:UIBarStyleDefault];
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *buttonItems = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [barItems addObject:buttonItems];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePickingStatus:)];
    
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    
    [barItems addObject:btnDone];
    [barItems addObject:btnCancel];
    [pickerToolbar setItems:barItems animated:YES];
    
    [pickerViewPopup addSubview:pickerToolbar];
    [pickerViewPopup addSubview:statusPickerView];
    
    [pickerViewPopup showInView:self.view];
    [pickerViewPopup setBounds:CGRectMake(0, 0, 320, 485)];
    
}

// return the number of columns to display
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// return the # of rows in each component...
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [statusList count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //return [NSString stringWithFormat:@"%i", row];
    return [statusList objectAtIndex:row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // set album status
    [[self Track] setValue:[statusList objectAtIndex:row] forKey:@"status"];
    
}


-(void)donePickingStatus:(id)sender
{
    // dismis the picker view
    [pickerViewPopup dismissWithClickedButtonIndex:0 animated:YES];
    
    pickerViewPopup = nil;
    
    statusPickerView = nil;
    
    // set the status value
    [LblStatus setText:[[self Track] valueForKey:@"status"]];
}

-(void)cancel:(id)sender
{
    [pickerViewPopup dismissWithClickedButtonIndex:0 animated:YES];
}


-(void)saveAlbum
{
    // save the changes to the  database
    // store data
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *yourArrayFileName = [documentsDirectory stringByAppendingPathComponent:@"Tracks.xml"];
    [Track writeToFile:yourArrayFileName atomically:YES];
}


-(void)serviceFinished:(id)service WithError:(BOOL)error
{
    // enter service response code
    if (!error)
    {
        if([service class] == [MusicSearchService class])
        {
            NSDictionary *track = [service details];
            [[self Track] setValue:[track valueForKey:@"releasedate"] forKey:@"releasedate"];
            
            // download cache album art image
            PictureDownloadService *service = [[PictureDownloadService alloc] init];
            [service setDelegate:self];
            [service setAlbumId:[[self Track] valueForKey:@"id"]];
            [service setAlbumPictureUrl: [[self Track]valueForKey:@"image"]];
            [serviceQueue addOperation:service];
            
            // refresh the UI in Main Thread
            [self performSelectorOnMainThread:@selector(refreshView) withObject:nil waitUntilDone:YES];
        }
        else
        {
            // refresh the UI in Main Thread
            [self performSelectorOnMainThread:@selector(refreshView) withObject:nil waitUntilDone:YES];
        }
    }
    else
    {
        /*
         [searchResults removeAllObjects];
         [searchResults addObject:
         [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"-1", @"There has been an Error", @"", nil] forKeys:[NSArray arrayWithObjects:@"id", @"name", @"artist",nil]]];
         [[self tableView] reloadData];
         */
    }

}

@end
