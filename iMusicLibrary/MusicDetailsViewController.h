//
//  MusicDetailsViewController.h
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 10/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicSearchService.h"
#import "ServiceDelegate.h"
#import "PictureDownloadService.h"



@interface MusicDetailsViewController : UIViewController <ServiceDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    NSOperationQueue *serviceQueue;
    
    UIActionSheet *pickerViewPopup;
    UIPickerView *statusPickerView;
    
    NSMutableArray *statusList;
    
    NSMutableArray *albumtracks;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgAlbumArt;
@property (weak, nonatomic) IBOutlet UILabel *LblalbumName;
@property (weak, nonatomic) IBOutlet UILabel *LblartistName;
@property (weak, nonatomic) IBOutlet UILabel *LblRealeaseYear;
@property (weak, nonatomic) IBOutlet UILabel *LblStatus;

@property (nonatomic, retain) NSMutableArray *albumtracks;

@property (weak, nonatomic) IBOutlet UITableView *AlbumTacksList;

@property (nonatomic, retain) IBOutlet NSDictionary *album;

@property (nonatomic, retain) NSMutableArray *statusList;

-(IBAction)changeStatus:(id)sender;

-(void)saveAlbum;

@end
