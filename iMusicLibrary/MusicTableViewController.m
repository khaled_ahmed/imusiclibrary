//
//  MusicTableViewController.m
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 06/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import "MusicTableViewController.h"


#define kCustomButtonHeight		30.0

// keys to our dictionary holding info on each page
#define kViewControllerKey		@"viewController"
#define kTitleKey				@"title"
#define kDetailKey				@"detail text"


@interface MusicTableViewController ()

@end

@implementation MusicTableViewController

@synthesize segmentOption;

@synthesize defaultTintColor;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
        
        // creat searchBar for the table header
        UIView *vw = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,40)];
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,320,40)];
        [vw addSubview:searchBar];
        [[self tableView] setTableHeaderView:vw];
        // set up search bar for uses
        [searchBar setPlaceholder:@"Album Title"];
        [searchBar setDelegate:self];
        searching = false;
        
        musics = [NSMutableArray arrayWithCapacity:10];
        searchResults = [NSMutableArray arrayWithCapacity:10];
        
        // set up service queue
        serviceQueue = [[NSOperationQueue alloc] init];
        [serviceQueue setMaxConcurrentOperationCount:1];
        
        
        // Restore and saved music lists
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDictionary = [paths objectAtIndex:0];
        NSString *yourArrayFileName = [documentsDictionary stringByAppendingPathComponent:@"Albums.xml"];
        
        musics = [[NSMutableArray alloc] initWithContentsOfFile:yourArrayFileName];
        
        if(musics == nil)
        {
            musics = [NSMutableArray arrayWithCapacity:10];
        }
        
        // sort albums
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        [musics sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
        
        // initialise the edit button
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
    }
    
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Album Lists";
    
    // segmented control as the custom title view
    [self LoadSegment];

}

-(void)LoadSegment
{
    NSArray *segmentTextContent = [NSArray arrayWithObjects:
                                   NSLocalizedString(@"All", @""),
                                   NSLocalizedString(@"I Own", @""),
                                   NSLocalizedString(@"Wanted", @""),
								   nil];
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
	segmentedControl.selectedSegmentIndex = 0;
	segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
	segmentedControl.frame = CGRectMake(0, 0, 200, kCustomButtonHeight);
	[segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    
	self.navigationItem.titleView = segmentedControl;

}

- (IBAction)segmentAction:(id)sender
{
	// The segmented control was clicked, handle it here
	UISegmentedControl *theSegmentedControl = (UISegmentedControl *)sender;
	NSLog(@"Segment clicked: %d", theSegmentedControl.selectedSegmentIndex);
    
    [self setSegmentOption:[theSegmentedControl selectedSegmentIndex]];

    [[self tableView] reloadData];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;
	
	// Before we show this view make sure the segmentedControl matches the nav bar style
	if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent || self.navigationController.navigationBar.barStyle == UIBarStyleBlackOpaque)
    {
		segmentedControl.tintColor = [UIColor darkGrayColor];
    }
	else
    {
		segmentedControl.tintColor = defaultTintColor;
    }
    
    // save film list one returened to list view
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *yourArrayFileName = [documentsDirectory stringByAppendingPathComponent:@"Albums.xml"];
    [musics writeToFile:yourArrayFileName atomically:YES];
    [[self tableView] reloadData];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SearchBar data souce

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // set the state to searching as true
    searching = true;
    
    // Add cancel/Done button to the Navigation bar
    [[self navigationItem] setRightBarButtonItem:
     [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(searchDone:)]];
    
    // Remove the edit button from the navigation bar
    [[self navigationItem] setLeftBarButtonItem:nil];
    
    // Remove the segment control from the navigation bar
    [[self navigationItem] setTitleView:nil];
    
    // Set navigation bar custom title
    [[self navigationItem] setTitle:@"Search for Album"];
    
    // for table to reload and redraw
    [searchResults removeAllObjects];
    [[self tableView] reloadData];
}

-(void)searchDone:(id)sender
{
    // clear search text
    [searchBar setText:@""];
    // hde the keyboard from the searchbar
    [searchBar resignFirstResponder];
    
    // Remove the Cancel/Done button from the navigation bar
    [[self navigationItem] setRightBarButtonItem:nil];
    
    // display the edit button
    [[self navigationItem] setLeftBarButtonItem:[self editButtonItem]];
    
    // display the segmented control
    [self LoadSegment];
    
    // clear search result and reset state
    searching = false;
    [searchResults removeAllObjects];
    
    // Force table to reload and redrew
    [[self tableView] reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)sb
{
    // Retrieve search term from search bar
    NSString *searchTerm = [searchBar text];
    
    
    MusicSearchService *service = [[MusicSearchService alloc] init];
    [service setSearchTerm:searchTerm];
    
    // set the search method name
    [service setSearchMethod:@"album"];
    
    [service setDelegate:self];
    
    
    [serviceQueue addOperation:service];
    
    [searchResults removeAllObjects];
    [searchResults addObject:
     [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"-1", @"Searching...", @"", nil] forKeys:[NSArray arrayWithObjects:@"id", @"name", @"artist", nil]]];
    [[self tableView] reloadData];
    
    // Hide keyboard from the searchBar
    [searchBar resignFirstResponder];
    
}

-(void)serviceFinished:(id)service WithError:(BOOL)error
{
    // enter service response code
    if (!error)
    {
        [searchResults removeAllObjects];
        
        for (NSDictionary *music in [service results])
        {
            // create dictionary to store multiple values for a album or singles
            int info_capacity = 6;
            NSMutableDictionary *m_info;
            m_info = [[NSMutableDictionary alloc] initWithCapacity:info_capacity];
            
            // store given variable
            [m_info setValue:[music valueForKey:@"id"] forKey:@"id"];
            [m_info setValue:[music valueForKey:@"name"] forKey:@"name"];
            [m_info setValue:[music valueForKey:@"artist"] forKey:@"artist"];
            [m_info setValue:[[[music valueForKey:@"image"] objectAtIndex:1] valueForKey:@"#text"] forKey:@"image"];
            [m_info setValue:[music valueForKey:@"mbid"] forKey:@"mbid"];
            [m_info setValue:@"I Own it" forKey:@"status"];
            
            // music info to main list
            [searchResults addObject:m_info];
        }
        
        // if there are no results found
        if([searchResults count] ==0)
        {
            [searchResults addObject:
             [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"-1", @"No Results Found", @"", nil] forKeys:[NSArray arrayWithObjects:@"id", @"name", @"artist", @"mbid", nil]]];
        }
        
        [[self tableView] reloadData];
    }
    else
    {
        [searchResults removeAllObjects];
        [searchResults addObject:
         [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"-1", @"There has been an Error", @"", nil] forKeys:[NSArray arrayWithObjects:@"id", @"name", @"artist", @"mbid",nil]]];
        [[self tableView] reloadData];
        
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return 10;
    
    return searching ? [searchResults count] : [musics count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *music;
    
    music = searching ? [searchResults objectAtIndex:[indexPath row]]:
                                    [musics objectAtIndex:[indexPath row]];
    if(segmentOption == 1)
    {
        if([[music valueForKey:@"status"] isEqualToString:@"I Own it"])
        {
            [[cell textLabel] setText:[music valueForKey:@"name"]];
            [[cell detailTextLabel] setText:[[music valueForKey:@"artist"] description]];
        }
        else
        {
            [[cell textLabel] setText:NULL];
            [[cell detailTextLabel] setText:NULL];
            //cell.userInteractionEnabled = false;

        }
    }
    else if(segmentOption == 2)
    {
        if([[music valueForKey:@"status"] isEqualToString:@"I want it"])
        {
            [[cell textLabel] setText:[music valueForKey:@"name"]];
            [[cell detailTextLabel] setText:[[music valueForKey:@"artist"] description]];
        }
        else
        {
            [[cell textLabel] setText:NULL];
            [[cell detailTextLabel] setText:NULL];
            //cell.userInteractionEnabled = false;
        }
    }
    else
    {
        [[cell textLabel] setText:[music valueForKey:@"name"]];
        [[cell detailTextLabel] setText:[[music valueForKey:@"artist"] description]];
    }
    return cell;
}


// row hight
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return !searching;
    //return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if row is deleted remove it from the list
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Remove from arrays
        NSDictionary * album = [musics objectAtIndex:[indexPath row]];
        
        // Delete Thumbnail if exist
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@.png", docDir, [album valueForKey:@"id"]];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if([fileManager fileExistsAtPath:pngFilePath])
        {
            [fileManager removeItemAtPath:pngFilePath error:nil];
        }
        
        // remove from the album list and save changes
        [musics removeObject:album];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *yourArrayFileName = [documentsDirectory stringByAppendingPathComponent:@"Albums.xml"];
        [musics writeToFile:yourArrayFileName atomically:YES];
        
        // trigger remove animation on table
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
    }
}

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    if(searching)
    {
        
        NSDictionary *music = [searchResults objectAtIndex:[indexPath row]];
        
        // check label for system message
        if([[music valueForKey:@"id"] intValue] != -1)
        {
            // add new music to list
            [musics addObject:music];
            
            // clear search text
            [searchBar setText:@""];
            
            // Remove the Cancel/Done Button from navigation bar
            [[self navigationItem] setRightBarButtonItem:nil];
            
            // load the segmented control
            [self LoadSegment];
            
            // display the edit button
            [[self navigationItem] setLeftBarButtonItem:[self editButtonItem]];
            
            // clear search results and reset state
            searching = false;
            [searchResults removeAllObjects];
            
            // force table to reload and redraw
            [[self tableView] reloadData];
            
            // sort albums
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            [musics sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            
            // store data
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *yourArrayFileName = [documentsDirectory stringByAppendingPathComponent:@"Albums.xml"];
            [musics writeToFile:yourArrayFileName atomically:YES];
        }
    }
    else
    {
        // use for interaction with film list
        NSDictionary *album = [musics objectAtIndex:[indexPath row]];
            
        MusicDetailsViewController *vc = [[MusicDetailsViewController alloc] initWithNibName:@"MusicDetailsViewController" bundle:nil];
            
        [vc setAlbum:album];
        
        [[self navigationController] pushViewController:vc animated:YES];
            
    }
    
}

@end
