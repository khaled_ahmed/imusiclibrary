//
//  AppDelegate.m
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 06/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import "AppDelegate.h"
#import "MusicTableViewController.h"
#import "TrackTableViewController.h"

@implementation AppDelegate

@synthesize window;
//@synthesize tabController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    // create view controllers
    MusicTableViewController *albumView = [[MusicTableViewController alloc] init];
    TrackTableViewController *trackView = [[TrackTableViewController alloc] init];
    AboutViewController *aboutView = [[AboutViewController alloc] init];
    
    // creat navigation controller and initialise the root view with table view controlles
    AboutNavcontroller = [[UINavigationController alloc] initWithRootViewController:aboutView];
    TrackNavcontroller = [[UINavigationController alloc]initWithRootViewController:trackView];
    //TrackNavcontroller.tabBarItem.title = @"Tracks";
    TrackNavcontroller.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Track" image:[UIImage imageNamed:@"music-tab.png"] tag:5];
    
    
    AlbumNavcontroller = [[UINavigationController alloc] initWithRootViewController:albumView];
    AlbumNavcontroller.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Album" image:[UIImage imageNamed:@"album-tab.png"] tag:4];
    
    AboutNavcontroller = [[UINavigationController alloc] initWithRootViewController:aboutView];
    AboutNavcontroller.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"About" image:[UIImage imageNamed:@"about-tab.png"] tag:3];
    // initialise a tab bar controller
    UITabBarController *tabController = [[UITabBarController alloc] init];
    tabController = [[UITabBarController alloc] init];
    
    // initialise controller lists in mutable array
    NSMutableArray *controllers = [[NSMutableArray alloc]initWithObjects:AlbumNavcontroller, TrackNavcontroller, AboutNavcontroller, nil];
    // assign the controller list in the tab controller view
    tabController.viewControllers = controllers;
    
    //tabController.viewControllers = [NSArray arrayWithObjects:AlbumNavcontroller, TrackNavcontroller, nil];
    // assign the tabController as the windows root view controller
    self.window.rootViewController = tabController;
    // end of custom code
    
    [self.window makeKeyAndVisible];
    return YES;
}

-(void)dealloc
{
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
