//
//  AppViewController.h
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 12/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AppViewController : UIViewController<UITabBarControllerDelegate>
{
    UITabBarController *tabController;
}


@property (nonatomic, strong) UITabBarController *tabController;

@end
