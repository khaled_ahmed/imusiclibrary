//
//  TrackDetailsViewController.h
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 11/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicSearchService.h"
#import "ServiceDelegate.h"
#import "PictureDownloadService.h"

@interface TrackDetailsViewController : UIViewController<ServiceDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSOperationQueue *serviceQueue;
    
    UIActionSheet *pickerViewPopup;
    UIPickerView *statusPickerView;
    
    NSMutableArray *statusList;
    
}


@property (weak, nonatomic) IBOutlet UIImageView *imgAlbumArt;
@property (weak, nonatomic) IBOutlet UILabel *LblTrackName;
@property (weak, nonatomic) IBOutlet UILabel *LblalbumName;
@property (weak, nonatomic) IBOutlet UILabel *LblartistName;
@property (weak, nonatomic) IBOutlet UILabel *LblRealeaseDate;
@property (weak, nonatomic) IBOutlet UILabel *LblStatus;

@property (weak, nonatomic) IBOutlet UITableView *AlbumTacksList;

@property (nonatomic, retain) IBOutlet NSDictionary *Track;

@property (nonatomic, retain) NSMutableArray *statusList;

-(IBAction)changeStatus:(id)sender;

-(void)saveAlbum;

@end
