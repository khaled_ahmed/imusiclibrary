//
//  PictureDownloadService.m
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 14/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import "PictureDownloadService.h"

@implementation PictureDownloadService

@synthesize albumId;
@synthesize albumPictureUrl;
@synthesize delegate;

-(void)main
{
    NSString *url = [self albumPictureUrl];
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@.png", docDir, albumId];
    NSURL *aURL = [NSURL URLWithString:url];
    NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
    UIImage *album_Image = [UIImage imageWithData:data];
    
    NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(album_Image)];
    [data1 writeToFile:pngFilePath atomically:YES];
    
    [delegate serviceFinished:self WithError:NO];
}

@end
