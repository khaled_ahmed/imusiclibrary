//
//  MusicSearchService.h
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 06/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceDelegate.h"

@interface MusicSearchService : NSOperation
{
    NSString *serchTerm;
    
    NSString *searchMethod;
    
    NSString *mBidId;
    
    NSString *album;
    NSString *artist;
    NSString *track;
    
    id<ServiceDelegate> delegate;
    
    NSArray *results;
    NSDictionary *details;
    NSDictionary *tracklists;
}

@property (nonatomic, retain) NSString *searchTerm;

@property (nonatomic, retain) NSString *searchMethod;

@property (nonatomic, retain) NSString *mBidId;

@property (nonatomic, retain) NSString *album;
@property (nonatomic, retain) NSString *artist;
@property (nonatomic, retain) NSString *track;

@property (nonatomic, retain) id<ServiceDelegate> delegate;

@property (nonatomic, retain) NSArray *results;

@property (nonatomic, retain) NSDictionary *details;

@property (nonatomic, retain) NSDictionary *tracklists;


@end
