//
//  AppDelegate.h
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 06/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicTableViewController.h"
#import "TrackTableViewController.h"
#import "AboutViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>
{
    UINavigationController *AlbumNavcontroller;
    UINavigationController *TrackNavcontroller;
    UINavigationController *AboutNavcontroller;
    UISegmentedControl *segmentOption;

}

@property (strong, nonatomic) UIWindow *window;

@end
