//
//  AppViewController.m
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 12/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import "AppViewController.h"

@interface AppViewController ()


@end

@implementation AppViewController

@synthesize tabController;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
