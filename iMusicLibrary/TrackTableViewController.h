//
//  TrackTableViewController.h
//  iMusicLibrary
//
//  Created by Shofiqur Rahman on 11/03/2013.
//  Copyright (c) 2013 Shofiqur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicSearchService.h"
#import "TrackDetailsViewController.h"

@interface TrackTableViewController : UITableViewController< UISearchBarDelegate, ServiceDelegate>
{
    UISearchBar *searchBar;
    
    BOOL searching;
    
    NSMutableArray *musics;
    NSMutableArray *searchResults;
    
    NSInteger segmentOption;
    
    NSOperationQueue *serviceQueue;
    
    // segment controll color
    UIColor *defaultTintColor;
}

@property (retain, nonatomic) UIColor *defaultTintColor;

@property (assign, nonatomic) NSInteger segmentOption;


@end
